<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Prueba de Manejo</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="FAME Chevrolet Apatzingán, Michoacán. Sitio WEB oficial FAME Chevrolet Apatzingán. Te brindamos información sobre Autos, Pick Up's, SUV's, Vans, Vehículos Comerciales y todos los servicios que Chevrolet® ofrece para tí. Piensa en auto, piensa en FAME. ">
    <meta name="keywords" content="Chevrolet Apatzingan, chevrolet cupatitzio, chevrolet cupatitzio apatzingan, chevrolet fame, fame cupatitzio, Grupo Fame apatzingan, cupatitzio apatzingan, fame cupatitzio apatzingan, chevrolet michoacan, autos usados apatzingan, autos en venta en apatzingan, fame apatzingan, fame Michoacan, México, Autos apatzingan, Nuevos apatzingan, Seminuevos apatzingan, Agencia apatzingan, Servicio apatzingan, Taller de servicio apatzingan, Hojalatería, hojalateria en apatzingan, chevrolet Fame Apatzingán, Pintura, postventa, chevrolet Matiz, chevrolet Spark, chevrolet aveo, chevrolet sonic, chevrolet cruze, chevrolet malibu, chevrolet camaro, chevrolet traverse, chevrolet tahoe, chevrolet suburban, chevrolet colorado, chevrolet silverado, chevrolet cheyenne, chevrolet 2015, chevrolet Lujo, chevrolet 2014, chevrolet 2015">
    <meta name="author" content="Mercadotecnia Grupo Fame División Automotriz">

	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css" media="screen">
    
    <link rel="icon" type="image/png" href="/images/favicon.png" />    

</head>
<body><em></em>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
		<!-- Header -->
		<header class="clearfix">
			<!-- Static navbar -->
			<div class="navbar navbar-default navbar-fixed-top">
				<div class="top-line">
					<div class="container">
						<p>
							<span><i class="fa fa-phone"></i>Agencia: (453) 534 0237</span>
<!--                            <span><i class="fa fa-user"></i><a href="aviso.html">Aviso de privacidad</a></span>-->
						</p>
						<ul class="social-icons">
                        
							<li><a class="whatsapp" href="contacto.php" target="_self"><i class="fa fa-whatsapp"></i></a></li>
							<li><a class="facebook" href="https://www.facebook.com/chevroletFame?fref=ts" target="_blank"><i class="fa fa-facebook"></i></a></li>
							<li><a class="twitter" href="https://twitter.com/chevroletfame" target="_blank"><i class="fa fa-twitter"></i></a></li>
							<li><a class="youtube" href="https://www.youtube.com/user/GrupoFameAutos" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                            <li><a class="instagram" href="https://www.instagram.com/grupofame/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                          </ul>
					</div>
				</div>
                
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="index.html" target="_self"><img alt="Inicio" src="images/logo.png"></a>
					</div>
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">

							<li><a href="index.html">Inicio</a></li>
							<li class="drop"><a href="autos.html">Autos</a>
								<ul class="drop-down">
									<li><a href="autos.html" target="_self">Autos</a></li>
									<li><a href="autos.html" target="_self">SUV</a></li>
									<li><a href="autos.html" target="_self">Pick Up's</a></li>
									<li><a href="autos.html" target="_self">Comerciales</a></li>																																				
                                 </ul> </li>                             
 
							<li class="drop"><a class="active" href="servicio.php">Servicio</a>
								<ul class="drop-down">
									<li><a href="servicio.php">Cita de Servicio</a></li>                     
									<li><a href="refacciones.php">Refacciones</a></li>
                                    
                                    <li><a href="garantia.html">Garantía</a></li>
                                  </ul> </li>                                                             
                        
							<li class="drop"><a href="promociones.html">Promociones</a>
                            	<ul class="drop-down">
                            		<li><a href="financiamiento.html">Financiamiento</a></li>
                            		<li><a href="servicio.html">Servicio</a></li>
                            	 </ul>  </li>     
                            <li><a href="http://www.fameseminuevos.com/" target="_blank">Seminuevos</a></li>                       
							<li><a href="contacto.php">Contacto</a></li>
                            <li><a href="ubicacion.html">Ubicación</a></li>
						</ul>
					</div>
				</div>
			</div>
		</header><br>
		<!-- End Header -->

		<!-- content 
			================================================== -->
		<div id="content">
        
        
<!-- ANALYTICS CHEVROLET APATZINGÁN-->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47755754-1', 'auto');
  ga('send', 'pageview');

</script>



<!-- FIN ANALYTICS -->            

		<!-- content 
			================================================== -->
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>Agenda tu Prueba de Manejo</h2>

				</div>
			</div>


			<!-- contact box -->
			<div class="contact-box">
				<div class="container">
					<div class="row">

						<div class="col-md-6" align="center">
						  <h3>Prueba de Manejo</h3>
                            
<?php
	if (isset($_POST['boton'])) {
        if($_POST['nombre'] == '') {
        	$errors[1] = '<span class="error">Ingrese su nombre</span>';
        } else if($_POST['email'] == '' or !preg_match("/^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/",$_POST['email'])){
        	$errors[2] = '<span class="error">Ingrese un email correcto</span>';
        } else if($_POST['telefono'] == '') {
        	$errors[3] = '<span class="error">Ingrese un teléfono</span>';
        } else if($_POST['mensaje'] == '') {
        	$errors[4] = '<span class="error">Ingrese un mensaje</span>';
        } else {
        	$dest = "mariaorozco@famecupatitzio.com, gerenciaapa@famecupatitzio.com" . ','. "formas@grupofame.com";
            $nombre = $_POST['nombre'];
            $email = $_POST['email'];
            $telefono = $_POST['telefono'];
			$asunto_cte = "Prueba de Manejo Chevroler FAME Apatzingán";
			$asunto = "Prueba de Manejo Chevrolet Apatzingán";
            $cuerpo = $_POST['mensaje'];
			$cuerpo_mensaje .= $nombre . "<br>" . "Mensaje: ". $cuerpo . "<br>" . "Mi correo es: " . $_POST['email'] . "<br>" . "Mi teléfono es: " . $_POST['telefono'];
			$cuerpo_cte = '
			<html>
			<head>
			  <title>Mail from '. $nombre .'</title>
			</head>
			<body>
			  <table style="width: 500px; font-family: arial; font-size: 14px;" border="1">
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Nombre:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $nombre .'</td>
				</tr>
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Correo:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $email .'</td>
				</tr>
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Mensaje:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $cuerpo .'</td>
				</tr>
			  </table>
			</body>
			</html>
			';
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
			$headers .= 'From: ' . $email . "\r\n";
            
            if(mail($dest,$asunto,$cuerpo_mensaje,$headers)){
            	$result = '<div class="result_ok">Email enviado correctamente, en breve nos comunicaremos contigo. Gracias por tu preferencia. "Piensa en Auto, Piensa en FAME"</div>';
				mail($email,$asunto_cte,$cuerpo_cte,$headers);
                $_POST['nombre'] = '';
                $_POST['email'] = '';
                $_POST['telefono'] = '';
                $_POST['mensaje'] = '';
            } else {
            	$result = '<div class="result_fail">Hubo un error al enviar el mensaje</div>';
            }
        }
    }
?>
<html>
	<meta charset="utf-8">
    <link rel='stylesheet'| href='css/formularios.css'>
    <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js'></script>
	<form id="contact-form" class="contact-work-form2" method='post' action=''>
    	<div class="text-input">
        	<div class="float-input">
            <input name='nombre' id="nombre" placeholder="Nombre*" type='text' class='nombre' value='<?php if(isset($_POST['nombre'])){ echo $_POST['nombre']; } ?>'> 
        <?php if(isset($errors)){ echo $errors[1]; } ?><span><i class="fa fa-user"></i></span></div>
        
        <div class="float-input2"><input name='email' placeholder="E-mail*" type='text' class='email' value='<?php if(isset($_POST['email'])){ echo $_POST['email']; } ?>'>
        <?php if(isset($errors)){ echo $errors[2]; } ?><span><i class="fa fa-envelope"></i></span></div>
        </div>
        
        <div class="text-input">
        <div class="float-input">
        <input name='telefono' id="telefono" placeholder="Teléfono*" type='tel' class='telefono' value='<?php if(isset($_POST['telefono'])){ echo $_POST['telefono']; } ?>'>
        <?php if(isset($errors)){ echo $errors[3]; } ?><span><i class="fa fa-phone"></i></span>
        </div>
        </div>
        
        <div class="textarea-input"><textarea name='mensaje' placeholder="Mensaje*" rows='5' class='mensaje'><?php if(isset($_POST['mensaje'])){ echo $_POST['mensaje']; } ?></textarea>
        <?php if(isset($errors)){ echo $errors[4]; } ?><span><i class="fa fa-comment"></i></span>
        </div>
        <div><input name='boton' type='submit' value='Enviar' class='boton'></div>
        <br><br><br>
<?php if(isset($result)) { echo $result; } ?>
    </form>                            
                            
<p align="justify">*Le recordamos que las citas de manejo y servicio están sujetas a horarios disponibles.</p>                            
                            
                            
                          <!--  
						<form id="contact-form" class="contact-work-form2">

								<div class="text-input">
									<div class="float-input">
										<input name="nombre" id="nombre" type="text" placeholder="Nombre*" required>
										<span><i class="fa fa-user"></i></span>
									</div>

									<div class="float-input2">
										<input name="mail" id="mail2" type="email" placeholder="Email*" required>
										<span><i class="fa fa-envelope"></i></span>
									</div>
								</div>
                                
								<div class="text-input">
									<div class="float-input">
										<input name="telefono" id="telefono" type="tel" placeholder="Teléfono*" required>
										<span><i class="fa fa-phone"></i></span>
									</div>

								</div>                                

								<div class="textarea-input">
									<textarea name="mensaje" id="mensaje" placeholder="Mensaje*" required></textarea>
									<span><i class="fa fa-comment"></i></span>
								</div>

								<div class="msg2 message"></div>
								<input type="submit" name="mailing-submit" class="submit_contact main-form" value="Enviar">

							</form>  -->
						</div>

					</div>
				</div>
			</div>

		</div><br><br><br><br><br>



		<!-- End content -->


		<!-- footer 
			================================================== -->
		<footer>
			<div class="footer-line">
				<div class="container">
					<p>2016 Chevrolet FAME Apatzingán | <span><i class="fa fa-phone"> </i> 01800 670 8386</span> | <a href="aviso.html" target="_self"><strong>  Aviso de privacidad</strong></a></p>
					<a class="go-top" href="#"></a>
				</div>
			</div>

		</footer>

		<!-- End footer -->
        
	</div>
	<!-- End Container -->

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
  	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript" src="js/gmap3.min.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/plugins-scroll.js"></script>
	<script type="text/javascript" src="js/script.js"></script>

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
</body>
</html>