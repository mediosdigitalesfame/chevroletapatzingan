<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Cita de Servicio</title>
	<?php include('contenido/head.php'); ?>
</head>
<body>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
		<?php include('contenido/header.php'); ?>
		<?php include('contenido/analytics.php'); ?>
		<div id="content">

			<div class="page-banner">
				<div class="container">
					<h2>Servicio Chevrolet FAME</h2>
				</div>
			</div>


			<!-- contact box -->
			<div class="contact-box">
				<div class="container">
						  <center><h3>Cita de Servicio</h3></center>
  <div class="col-md-3" align="center">
					</div>     	 
                    <div class="col-md-6" align="center">
							<?php include('form.php'); ?>
					</div>
				</div>
				<div class="section">
			<div id="about-section">
<br><br>

				<div class="welcome-box">
					<div class="container">
						<h1><span>Valor es el mejor precio todo el año</span></h1><br>
						<p align="justify">En Chevrolet Servicio® la relación con nuestros clientes no termina con la venta, sino comienza con ella. Por eso, para brindar un servicio de gran calidad, contamos con la más completa red de concesionarios distribuidos por todo el país, con el trato personal y la más alta tecnología que sólo se puede esperar de una marca líder, como es Chevrolet®.
Al venir con nuestros técnicos, usted podrá sentirse seguro de su desempeño, ya que están constantemente actualizándose por medio de un programa de capacitación. Programa que también incluye a nuestro consultor de servicio y nuestra mano de obra calificada, con la cual usted estará experimentando un servicio de expertos, ya que se encuentra en constante preparación para resolver cualquier inconveniente posible, sumado a:</p><br>
                 
<p align="justify">
- Una atención personalizada<br>
- Refacciones originales<br>
- Herramientas especializadas<br>
- Centros de servicio con la tecnología adecuada<br>
- Diagnósticos específicos para tu Chevrolet®</p><br><br>

<!-- <p align="left">Te invitamos a conocer los programas y promociones que hemos diseñado especialmente para ti.</p><br>                        

<br><br>


<br>

						 <h1><span>Precios de Mantenimiento</span></h1><br>
							<div class="single-project-content">
                            	
                            	<div class="col-md-6">
								<img alt="" src="images/servicio-basico1.jpg">
                                </div>
                               
                                <div class="col-md-6">
								<img alt="" src="images/servicio-basico2.jpg">
                                </div>
                               	<br></div></div>
                                
                              <div class="container"><p align="justify"><br>
                              <strong>Publicación Servicio Básico modelos 2014 - 2017</strong><br><br>
                               
                              A partir de años modelo 2017 la frecuencia de servicio es cada 12,000 km o 12 meses, lo que ocurra primero. Precios incluyen aceite de motor sintético DEXOS, refacciones (original o ACDelco según aplique) y mano de obra de acuerdo a la Póliza de Garantía y Programa de Mantenimiento del vehículo. Todos los precios son en Moneda Nacional e incluyen el Impuesto al Valor Agregado. Precios válidos en los Estados Unidos Mexicanos del 16 de enero al 30 de junio de 2017. Precios sugeridos por General Motors de México S. de R.L. de C.V. Para información de otros vehículos, aplicabilidad o de las garantías correspondientes a los productos y servicios pregunte a su Distribuidor Autorizado Chevrolet.
D.R. © General Motors de México S. de R.L. de C.V., Av. Ejército Nacional 843, Col. Granada, C.P. 11520, Ciudad de México, 2017.<br><br>

							<strong>Publicación Servicio Básico modelos 2013</strong><br><br>
Precios incluyen aceite de motor sintético DEXOS, refacciones (original o ACDelco según aplique) y mano de obra de acuerdo a la Póliza de Garantía y Programa de Mantenimiento del vehículo. (*)Sujeto al diagnóstico del técnico puede requerir bujías antes de 150,000 km. Todos los precios son en Moneda Nacional incluyen el Impuesto al Valor Agregado. Precios válidos en los Estados Unidos Mexicanos del 16 de enero al 30 de junio de 2017. Precios sugeridos por General Motors de México S. de R.L. de C.V. Para información de otros vehículos, aplicabilidad o de las garantías correspondientes a los productos y servicios pregunte a su Distribuidor Autorizado Chevrolet.<br>
D.R. © General Motors de México S. de R.L. de C.V., Av. Ejército Nacional 843, Col. Granada, C.P. 11520, Ciudad de México, 2017.
                         	</p> </div>
                            
                    <div class="services-box">
					<div class="container">
						<div class="row">
							<div class="col-md-6" >
								<div class="services-post">
									<a class="services-icon1" href="images/aceite.jpg" target="_blank"><i class="fa fa fa-automobile"></i></a>
									<div class="services-post-content">
										<h4><strong>CAMBIO DE ACEITE <br> Y FILTRO</strong></h4>
										<p>Pregunta a tu Distribuidor Chevrolet más cercano por el cambio de aceite y filtro desde <strong>$699.</strong></p>
									</div>
								</div>
							</div>
                            
							<div class="col-md-6" >
								<div class="services-post">
									<a class="services-icon1" href="images/frenos.jpg" target="_blank"><i class="fa fa-caret-down"></i></a>
									<div class="services-post-content">
										<h4><strong>MANTENIMIENTO FRENOS</strong></h4>
										<p>Entra a éste enlace para ver los precios de mantenimineto de tus frenos</p>
									</div>
								</div>
							</div>                            

							
                            
							
						</div>
					</div>
					<img class="shadow-image" alt="" src="images/shadow.png">
				</div>

</div>


</div></div>

<?php include('contenido/footer.php'); ?>

</body>
</html>