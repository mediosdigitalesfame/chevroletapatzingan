<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="Title" content="FAME Chevrolet Apatzingán, Michoacán.">
	<meta name="description" content="Sitio Oficial de Chevrolet Apatzingan Información sobre Autos, Camionetas, Pick Up's, SUV's, Vans y todos los servicios que ofrecemos para tí. Piensa en auto, piensa en FAME. ">
    <meta name="keywords" content="Chevrolet Apatzingan, chevrolet cupatitzio, chevrolet cupatitzio apatzingan, chevrolet fame, fame cupatitzio, Grupo Fame apatzingan, cupatitzio apatzingan, fame cupatitzio apatzingan, chevrolet michoacan, autos usados apatzingan, autos en venta en apatzingan, fame apatzingan, fame Michoacan, México, Autos apatzingan, Nuevos apatzingan, Seminuevos apatzingan, Agencia apatzingan, Servicio apatzingan, Taller de servicio apatzingan, Hojalatería, hojalateria en apatzingan, chevrolet Fame Apatzingán, Pintura, postventa, chevrolet Matiz, chevrolet Spark, chevrolet aveo, chevrolet sonic, chevrolet cruze, chevrolet malibu, chevrolet camaro, chevrolet traverse, chevrolet tahoe, Chevrolet suburban, chevrolet colorado, chevrolet silverado, chevrolet cheyenne, chevrolet 2016, chevrolet Lujo, chevrolet 2016, chevrolet 2016">
    <meta name="author" content="Mercadotecnia Grupo Fame División Automotriz">
    
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/magnific-popup.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/fullwidth.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/settings.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.css" media="screen">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css" media="screen">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/flexslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/responsive.css" media="screen">
   

    <link rel="icon" type="image/png" href="/images/favicon.png" />
    
    <!-- Inserta esta etiqueta en la sección "head" o justo antes de la etiqueta "body" de cierre. -->
<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'es-419'}
</script>
